FROM openjdk:8u212-jre-alpine3.9
COPY target/*.jar booking.jar
EXPOSE 9000
CMD java -jar booking.jar