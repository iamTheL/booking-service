package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;

@org.springframework.web.bind.annotation.RestController
public class RestController {
	
//	@RequestMapping()
	@GetMapping
	public String controller()
	{
		return "I am booking service";
	}
	
	@GetMapping("/book")
	public String controller1()
	{
		return "I am still booking service";
	}

}
